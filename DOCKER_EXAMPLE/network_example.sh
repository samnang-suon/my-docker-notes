# source:
# https://docs.docker.com/network/bridge/

###################################################################################################
# EXAMPLE 01
###################################################################################################
# First, start from a clean state
sudo docker container stop $(sudo docker container ls -aq)
sudo docker container prune --force && \
sudo docker image prune --force &&
sudo docker network prune --force && \
sudo docker volume prune --force && \
sudo docker system prune --force --all

# Create a network
sudo docker network create my-secret-network

# List our network
sudo docker network ls

# Create a container with our network
sudo docker run -it --rm --network my-secret-network --name my-secret-alpine alpine

# Show our container ip address
ifconfig

# Create another container with our network (in another terminal)
sudo docker run -it --rm --network my-secret-network --name my-secret-busybox busybox
ifconfig

# From Busybox, ping Alpine
ping [AlpineIpAddress]
# That is it! This is how to make 2 or more containers talk to each other.


# Open another terminal and run
sudo docker network inspect my-secret-network
# At the end, you should see our 2 containers:
"Containers": {
    "1ed7e399d81607dd9680e7aee44b82a766dad1df90d1bea4f6f7394bb5f717b9": {
        "Name": "my-secret-busybox", <-----
        "EndpointID": "f9813e7455f1d381d36379b0838a429f17ea297465b7d41d1395c45f94eb2c7e",
        "MacAddress": "02:42:ac:16:00:03",
        "IPv4Address": "172.22.0.3/16",
        "IPv6Address": ""
    },
    "93e63e5912761a6805db0903e0956e9217415fb6ca13fda2e1fb1dbbd5635641": {
        "Name": "my-secret-alpine", <-----
        "EndpointID": "6c4b44169a78ab0d081ca19ba5c7c02be95de5ad13700a76b51f1636b92685f0",
        "MacAddress": "02:42:ac:16:00:02",
        "IPv4Address": "172.22.0.2/16",
        "IPv6Address": ""
    }
},
###################################################################################################
# EXAMPLE 02
###################################################################################################
# First, start from a clean state
sudo docker container stop $(sudo docker container ls -aq)
sudo docker container prune --force && \
sudo docker image prune --force &&
sudo docker network prune --force && \
sudo docker volume prune --force && \
sudo docker system prune --force --all

# Create a network
sudo docker network create my-secret-network

# List our network
sudo docker network ls

# Create a container with our network
sudo docker run -d --rm --network my-secret-network --name my-secret-nginx nginx

# List our container (To see that it is still running)
sudo docker container ls

# Find the IPv4 of our Nginx container
sudo docker inspect [NginxContainerID]
# Look at the "IPAddress"
# Example:
# Gateway": "172.17.0.1",
# "IPAddress": "172.17.0.2" <----- This is what we want

# Create another container with our network (in another terminal)
sudo docker run -it --rm --network my-secret-network --name my-secret-ubuntu ubuntu
apt update && apt install curl inetutils-ping --yes

# Verify that Ubuntu can talk to Nginx
curl [NginxIpAddress]
ping [NginxIpAddress]
# That is it! This is how to make 2 or more containers talk to each other.

# Open another terminal and run:
sudo docker network inspect my-secret-network
# At the end, you should see our 2 containers:
"Containers": {
    "ce344cf21bc01c04a71d4a529918d8d43febaf8c98073473b3af5b34b37c55e8": {
        "Name": "my-secret-nginx", <-----
        "EndpointID": "9aab38c4abc2837ac42700e2867ce3c8cc2d9527c88491c21e2d5bb12917f7cf",
        "MacAddress": "02:42:ac:17:00:02",
        "IPv4Address": "172.23.0.2/16",
        "IPv6Address": ""
    },
    "da099dcd1a94dee7f36dd98b8e498591ac6565bb3f44090345a53d970a7ff8a9": {
        "Name": "my-secret-ubuntu", <-----
        "EndpointID": "50efd851d4979a0ddfba0649e1ddc42c075fd8f7a62df619a2d3fbfc511bd6c1",
        "MacAddress": "02:42:ac:17:00:03",
        "IPv4Address": "172.23.0.3/16",
        "IPv6Address": ""
    }
},
