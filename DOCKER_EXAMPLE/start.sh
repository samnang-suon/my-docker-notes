###################################################################################################
# How to build this Dockerfile
sudo docker build --tag my-todo-app ./TodoApp
# To view your new image
sudo docker images
# OR
sudo docker image ls


###################################################################################################
# How to run the container
sudo docker run -d \
    --publish 3000:3000 \
    --name 1st_instance_todo_app \
    my-todo-app
# To view your new app
# open a web browser and visit 'localhost:3000'
# Or 
curl localhost:3000
# To see your running container
sudo docker container ls


###################################################################################################
# How to persist data using named volume
# 0. Stop and delete previous container
sudo docker container stop 1st_instance_todo_app
sudo docker container rm 1st_instance_todo_app
# 1. Create a named volume
sudo docker volume create todo-db
# 2. View to new created volume
sudo docker volume ls
# 3. Run a container and attach the volume to it
sudo docker run -d \
      --publish 3000:3000 \
      --volume todo-db:/etc/todos \
      --name persist_todo_app \
      my-todo-app
# 4. Stop and delete that persit container
sudo docker container stop persist_todo_app
sudo docker container rm persist_todo_app
# 5. Create a new container using our existing volume
sudo docker run -d \
      --publish 3000:3000 \
      --volume todo-db:/etc/todos \
      --name spawn_persist_todo_app \
      my-todo-app
# 6. Visit 'localhost:3000'. You should see your saved data/items


###################################################################################################
# How to persist data using bind mount
sudo docker run -d \
      --publish 3000:3000 \
      --workdir /app \
      --volume "$(pwd):/app" \
      node:12-alpine \
      sh -c "yarn install && yarn run dev"


###################################################################################################
# Multi-container app
# 1. Create a network
sudo docker network create todo-app-bridge
# 2. View your newly created network
sudo docker network ls
# 3. Create a MySQL container and attach it to the network
sudo docker run -d \
      --network todo-app-bridge \
      --network-alias mysql \
      --volume todo-mysql-data:/var/lib/mysql \
      -e MYSQL_ROOT_PASSWORD=secret \
      -e MYSQL_DATABASE=todos \
      --name my_demo_db \
      mysql:5.7
# 4. View your running MySQL instance
sudo docker container ls
# 5. Connect to the database
sudo docker exec -it my_demo_db mysql -u root -p
# 6. List existing databases and exit
SHOW DATABASES;exit;
# 7. Create another container on the same network
sudo docker run -it --network todo-app-bridge --name my_netshoot nicolaka/netshoot
# 8. List network info and exit
dig mysql;exit;


###################################################################################################
# Run Todo App with MySQL
# 0. Stop and delete previous containers
sudo docker container stop spawn_persist_todo_app
sudo docker container rm spawn_persist_todo_app
sudo docker container stop my_demo_db
sudo docker container rm my_demo_db
sudo docker container stop my_netshoot
sudo docker container rm my_netshoot
# 1. Run app with database
sudo docker run -d \
      --publish 3000:3000 \
      --workdir /app \
      --volume "$(pwd):/app" \
      --network todo-app-bridge \
      -e MYSQL_HOST=mysql \
      -e MYSQL_USER=root \
      -e MYSQL_PASSWORD=secret \
      -e MYSQL_DB=todos \
      --name my_app_db \
      node:12-alpine \
      sh -c "yarn install && yarn run dev"
# To view our saved data
sudo docker exec -it <MyContainerID> -p todos
select * from todo_items;


###################################################################################################
# Clean up
# 0. List existing container
sudo docker container ls --all
# 1. Stop all running containers
sudo docker stop $(sudo docker ps -aq)
# OR
sudo docker container stop <ContainerID_01> <ContainerID_02> ... <ContainerID_N>
# 2. Delete all stopped/exited containers
sudo docker system prune
# 3. List existing images
sudo docker image ls --all
# 4. Delete all images
sudo docker system prune --all --force
# AL-IN-ONE
printf "\n\n>>> STOP ALL RUNNING CONTAINERS:\n" && \
sudo docker stop $(sudo docker ps -aq) && \
printf "\n\n>>> START DOCKER CLEAN UP:\n" && \
sudo docker container prune --force && \
sudo docker image prune --force && \
sudo docker volume prune --force && \
sudo docker network prune --force && \
sudo docker system prune --all --force && \
printf "\n\n>>> DOCKER IMAGES:\n" && \
sudo docker image ls --all && \
printf "\n\n>>> DOCKER CONTAINERS:\n" && \
sudo docker container ls --all


###################################################################################################
# DOCKER-COMPOSE
###################################################################################################
# 0. Verify that docker-compose was installed
sudo docker-compose version
# 1. Launch docker services
sudo docker-compose up -d
# 2. List running containers
sudo docker container ls --all
# 3. List existing images
sudo docker image ls --all
# 4. List existing network
sudo docker network ls
# 5. List existing volume
sudo docker volume ls
# To test, visit 'localhost:3000'
# To attach to MySQL and see the database content
# sudo docker attach docker_example_mysql_1 mysql -u root -p <----- This does not work for some reason
sudo docker exec -it docker_example_mysql_1 mysql -u root -p
show databases;
use todos;
show tables;
select * from todo_items;
exit;
