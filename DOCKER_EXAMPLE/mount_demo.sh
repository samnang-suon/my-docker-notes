# source:
# https://hub.docker.com/_/python
sudo docker run -it --volume "$PWD":/home --workdir /home alpine

# Write to a file
echo "Hello from Alpine" >> test.txt
exit

# After you exit, you should see a new file 'test.txt'