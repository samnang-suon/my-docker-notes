# source:
# https://docs.docker.com/storage/volumes/


# First, start from a clean state
sudo docker container prune --force && \
sudo docker image prune --force &&
sudo docker network prune --force && \
sudo docker volume prune --force && \
sudo docker system prune --force --all

# Create a volume
sudo docker volume create my-data-volume

# Start a container with our volume
sudo docker run -it --rm --volume my-data-volume:/tmp alpine

# Write some data to a file
cd tmp/
echo "Hello from Alpine!!!" > bonjour.txt

# Exit the container
exit

# List our volume
sudo docker volume ls

# Inspect our volume (to get the mountpoint - where our data is saved)
sudo docker inspect my-data-volume

# List the content of our volume
ls [MountpointPathHere]
# As you can see, even though our container was deleted (--rm), our data is still safe!

# We can attach our volume to a new container
sudo docker run -it --rm --volume my-data-volume:/tmp busybox

# Append data to our existing file
cd tmp/
echo "Hello from Busybox!!!" >> bonjour.txt
cat bonjour.txt
