## First, make sure to start in a clean state
sudo docker container stop $(sudo docker container ls -aq)
sudo docker container prune --force && \
sudo docker image prune --force && \
sudo docker network prune --force && \
sudo docker volume prune --force && \
sudo docker system prune --force --all

# Verify that docker-compose is installed
sudo docker-compose version

# List local images
sudo docker image ls

# Build the services
sudo docker-compose build

# Start services in detach mode
sudo docker-compose up -d

# List running containers
sudo docker container ls