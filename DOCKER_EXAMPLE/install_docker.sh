# Install Docker on Ubuntu
# source: https://docs.docker.com/engine/install/ubuntu/

# Update apk package index
sudo apt-get update

# Install HTTPS Tools
sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release --yes

# Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Set up the stable repository
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install Docker Engine
sudo apt-get update && sudo apt-get install docker-ce docker-ce-cli containerd.io --yes

# Verify Docker version
sudo docker --version
