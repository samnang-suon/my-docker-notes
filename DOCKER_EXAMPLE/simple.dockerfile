FROM ubuntu
LABEL maintainer=samnangsuon.ss@gmail.com
ARG AUTHOR=samnang
ENV YEAR=2021
RUN apt update
# ADD
# COPY
WORKDIR /tmp
# VOLUME
# EXPOSE
ENTRYPOINT ["echo", "Hello"]
CMD [" World!"]

# How to build this file
# sudo docker build --tag simple-demo --file path/to/dockerfile .
# How to start container
# sudo docker run simple-demo
# You should see:
# Hello World!
# sudo docker run simple-demo Alice
# You should see:
# Hello Alice