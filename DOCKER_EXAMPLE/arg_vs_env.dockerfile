FROM alpine
ARG AUTHOR=samnang
ENV YEAR=2021
RUN echo $AUTHOR
RUN echo $YEAR
ENTRYPOINT echo $AUTHOR $YEAR

# How to build this image
# sudo docker build --tag arg-env-demo --build-arg AUTHOR=suon --build-arg YEAR=2100 --file arg_vs_env.dockerfile .

# Run simple
# sudo docker run -it arg-env-demo
# You should see:
# 2021

# Run with ENV
# sudo docker run -it --env AUTHOR=John_Smith --env YEAR=12345 arg-env-demo
# You should see:
# John_Smith 12345