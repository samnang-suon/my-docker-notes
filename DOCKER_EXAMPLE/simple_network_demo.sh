# Start from a clean state
sudo docker container stop $(sudo docker container ls -aq)
sudo docker container prune --force && \
sudo docker image prune --force && \
sudo docker network prune --force && \
sudo docker volume prune --force && \
sudo docker system prune --force --all

# Start Alpha container
sudo docker run -d --name alpha alpine tail -F anything

# Start Bravo container
sudo docker run -d --name bravo alpine tail -F anything

# Make sure that BOTH containers are still running
sudo docker container ls

# Find Alpha IPv4 address
sudo docker container inspect alpha | grep "IPAddress"

# Attach to Bravo and ping Alpha
sudo docker exec -it bravo sh
ping [AlphaIPv4Address]
