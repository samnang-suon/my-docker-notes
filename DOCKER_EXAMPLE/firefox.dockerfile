# source:
# https://docs.docker.com/engine/reference/builder/#dockerfile-examples
FROM ubuntu

RUN apt-get update

# Install vnx and xvfb in order to fake display for Firefox
RUN apt-get install -y x11vnc xvfb firefox
RUN mkdir ~/.vnc

# Setup a password
RUN x11vnc -storepasswd 1234 ~/.vnc/passwd

# Autostart Firefox
RUN bash -c 'echo "firefox" >> /.bashrc'

# Expose a port to the outside world
EXPOSE 5900

CMD ["x11vnc", "-forever", "-usepw", "-create"]

# How to build this file
# sudo docker build --file firefox.dockerfile --tag my-firefox .

# List all available images
# sudo docker images
# OR
# sudo docker image ls

# Start a container from my-firefox image
# sudo docker run -d --publish 9090:5900 my-firefox

# Verify that our container is running
# sudo docker container ls