FROM alpine
CMD ["echo", "Hello from CMD1"]
ENTRYPOINT ["echo", "Hello from entrypoint"]
CMD ["echo", "Hello from CMD2"]

# How to build this image
# sudo docker build --tag cmd-entrypoint-demo --file cmd_vs_entrypoint.dockerfile .

# Run
# sudo docker run --rm cmd-entrypoint-demo
# You should see:
# Hello from entrypoint echo Hello from CMD2