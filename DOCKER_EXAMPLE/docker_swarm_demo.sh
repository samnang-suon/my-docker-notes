sudo docker swarm init
docker swarm join --token SWMTKN-1-2z1ucb8blr25b0mhezurl8npkdtd8blv0kvrh2fhbftbicwom3-a8iw44d5q41p830h8i2xi3cao 192.168.72.129:2377 <--- This will need to be executed by the worker node.


# You can always get back the Manager Token using:
sudo docker swarm join-token worker
docker swarm join --token SWMTKN-1-2z1ucb8blr25b0mhezurl8npkdtd8blv0kvrh2fhbftbicwom3-a8iw44d5q41p830h8i2xi3cao 192.168.72.129:2377
# Machine that uses the above token will join as a worker node.


# Generate a token for machine that want to join as Manager Node too:
sudo docker swarm join-token manager
docker swarm join --token SWMTKN-1-2z1ucb8blr25b0mhezurl8npkdtd8blv0kvrh2fhbftbicwom3-74vqhxrme34siijksuavra34r 192.168.72.129:2377


###################################################################################################
# The following commands can only be run inside the manager node.
# If you ran inside a worker node, you will see the following:
# """
# Error response from daemon: This node is not a swarm manager.
# Worker nodes can't be used to view or modify cluster state.
# Please run this command on a manager node or promote the current node to a manager.
# """
###################################################################################################
# Service visualizer
# source:
# https://hub.docker.com/r/dockersamples/visualizer
sudo docker service create \
  --name=viz \
  --publish=8080:8080/tcp \
  --constraint=node.role==manager \
  --mount=type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
  dockersamples/visualizer

# Show existing nodes
sudo docker node ls
# Example:
# ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
# jbppcfzchzzwvloix8xtzr1vo *   ubuntu     Ready     Active         Leader           20.10.6

# View Swarm Visualizer
sudo docker node inspect [ContainerID] | grep "Addr"
# Example:
# sudo docker node inspect jbppc | grep "Addr"
# "Addr": "192.168.72.129"
# "Addr": "192.168.72.129:2377"
# Then, open a web browser and visit 192.168.72.129:8080

# To launch container (task) on manager node
sudo docker service create --replicas=2 nginx
# Then, open a web browser and visit 192.168.72.129:8080
# You should see 2 more containers

# To update a service
## 1. List existing services
sudo docker service ls
# Example:
# ID             NAME              MODE         REPLICAS   IMAGE                             PORTS
# k107owddngeh   distracted_boyd   replicated   2/2        nginx:latest
# xjvxy176cx3v   viz               replicated   1/1        dockersamples/visualizer:latest   *:8080->8080/tcp
## 2. Scale existing services
sudo docker service update --replicas=1 <ServiceID>
# Example:
# sudo docker service update --replicas=1 k107owddngeh
# k107owddngeh
# overall progress: 1 out of 1 tasks
# 1/1: running   [==================================================>]
# verify: Service converged
## 3. Show after updates
sudo docker service ls
# Example:
# ID             NAME              MODE         REPLICAS   IMAGE                             PORTS
# k107owddngeh   distracted_boyd   replicated   1/1 <----  nginx:latest
# xjvxy176cx3v   viz               replicated   1/1        dockersamples/visualizer:latest   *:8080->8080/tcp

# As a worker node, you can leave the swarm using:
sudo docker swarm leave --force
# As a manager node, you can leave the swarm using:
sudo docker swarm leave --force
