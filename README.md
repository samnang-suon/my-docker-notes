# 05h19m_PLURALSIGHT_Docker for Web Developers Dan Wahlin
## Docker Layer
![ImageContainerLayer](images/ImageContainerLayer.png)

## Container and Volume
![DockerVolume](images/DockerVolume.png)

![DockerVolumeOverview](images/DockerVolumeOverview.png)

![CreatingDataVolume](images/CreatingDataVolume.png)

which translate to: sudo docker run -publish [HostPort]:[ContainerPort] --volume [ContainerVolume] [ImageName]

Q: Demonstrate the volume keeps data even after the container was deleted ?  
A:
```shell
# source:
# https://docs.docker.com/storage/volumes/


# First, start from a clean state
sudo docker container stop $(sudo docker container ls -aq)
sudo docker container prune --force && \
sudo docker image prune --force &&
sudo docker network prune --force && \
sudo docker volume prune --force && \
sudo docker system prune --force --all

# Create a volume
sudo docker volume create my-data-volume

# Start a container with our volume
sudo docker run -it --rm --volume my-data-volume:/tmp alpine

# Write some data to a file
cd tmp/
echo "Hello from Alpine!!!" > bonjour.txt

# Exit the container
exit

# List our volume
sudo docker volume ls

# Inspect our volume (to get the mountpoint - where our data is saved)
sudo docker inspect my-data-volume

# List the content of our volume
ls [MountpointPathHere]
# As you can see, even though our container was deleted (--rm), our data is still safe!

# We can attach our volume to a new container
sudo docker run -it --rm --volume my-data-volume:/tmp busybox

# Append data to our existing file
cd tmp/
echo "Hello from Busybox!!!" >> bonjour.txt
cat bonjour.txt
```

![LocatingDockerVolume](images/LocatingDockerVolume.png)

![CustomVolumeLocation](images/CustomVolumeLocation.png)

![LocatingCustomDockerVolume](images/LocatingCustomDockerVolume.png)

## Bind Mount
Q: Give an example of bind mount ?  
A: sudo docker run -it --rm --volume "$(pwd)":/tmp alpine

Q: How can you put your source code into a container ?  
A:
1. Create a container volume that point to the source code folder:  
sudo docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app node:8 node your-daemon-or-script.js  
source: https://github.com/nodejs/docker-node/blob/main/README.md#run-a-single-nodejs-script

2. Add your source code to a docker image layer using a Dockerfile
```dockerfile
FROM node:10
MAINTAINER samnangsuon.ss@gmail.com
ENV NODE_ENV=production
ENV PORT=3000
COPY . /var/www
WORKDIR /var/www
// VOLUME
RUN npm install
EXPOSE $PORT
ENTRYPOINT ["node", "server.js"]
```
## Dockerfile
Dockerfile + docker build = docker image  
just like:  
C++ source code + gcc = executable program

![DockerfileInstruction](images/DockerfileInstruction.png)

![DockerfileInstructionLatest](images/DockerfileInstructionLatest.png)  
Reference:
https://docs.docker.com/engine/reference/builder/

The most useful are:
```dockerfile
FROM
LABEL
ARG
ENV
COPY
RUN
WORKDIR
VOLUME
EXPOSE
ENTRYPOINT
CMD
```

## Build a Dockerfile
![BuildingCustomImage](images/BuildingCustomImage.png)
### Building a custom Dockerfile
    sudo docker build --tag demo-py --file python.dockerfile .
**NOTE** Do not forget the dot(.) at the end

## ADD vs COPY
* COPY takes in a src and destination. It only lets you copy in a local file or directory from your host (the machine building the Docker image) into the Docker image itself.
* ADD lets you do that too, but it also supports 2 other sources. First, you can use a URL instead of a local file / directory. Secondly, you can extract a tar file from the source directly into the destination.

Reference:
https://nickjanetakis.com/blog/docker-tip-2-the-difference-between-copy-and-add-in-a-dockerile

## ARG vs ENV
![DockerfileArgVsEnv](images/DockerfileArgVsEnv.png)  
Reference:
https://vsupalov.com/docker-arg-vs-env/

## CMD vs ENTRYPOINT
![DockerfileCmdVsEntrypoint](images/DockerfileCmdVsEntrypoint.png)  
Reference:
https://docs.docker.com/engine/reference/builder/#understand-how-cmd-and-entrypoint-interact

## Docker compose
![DockerComposeFeatures](images/DockerComposeFeatures.png)

![TheRoleOfTheDockerComposeFile](images/TheRoleOfTheDockerComposeFile.png)

![DockerComposeKeyInstructions](images/DockerComposeKeyInstructions.png)

![DockerComposeKeyCommands](images/DockerComposeKeyCommands.png)

```dockerfile
version: '3'

services:
  my-node-app:
    build:
      context: .
      dockerfile: node.dockerfile
    ports:
      - "3000:3000"
    networks:
      - my-secret-network

  my-mongo-database:
    image: mongo
    networks:
      - my-secret-network

networks:
  my-secret-network:
    driver: bridge
```

# ==================================================
# Docker Architecture
![DockerArchitecture](images/DockerArchitecture.png)

# Docker Object Model
After running the "sudo docker", you should see something like the following:

| Docker Object | Description |
|---------------|-------------|
config      | Manage Docker configs
container   | Manage containers
image       | Manage images
network     | Manage networks
secret      | Manage Docker secrets
service     | Manage services
system      | Manage Docker
volume      | Manage volumes

## Trick = "INVCS"
I = Image
N = Network
V = Volume
C = Container
S = System

That is why, the master command to do a full clean up is:

    sudo docker container stop $(sudo docker container ls -aq)
    sudo docker container prune --force && \
    sudo docker image prune --force && \
    sudo docker network prune --force && \
    sudo docker volume prune --force && \
    sudo docker system prune --force --all

# Docker Engine
![DockerEngineArchitecture](images/DockerEngineArchitecture.png)

# Docker Universal Control Plane (UCP)
![DockerUniversalControlPlaneArchitecture](images/DockerUniversalControlPlaneArchitecture.png)
```shell
# Pull the latest version of UCP
$ docker image pull docker/ucp:2.2.3

# Install UCP
$ docker container run --rm -it --name ucp \
  -v /var/run/docker.sock:/var/run/docker.sock \
  docker/ucp:2.2.3 install \
  --host-address <node-ip-address> \
  --interactive
```
references:
* https://hub.docker.com/r/docker/ucp
* http://docs.docker.oeynet.com/datacenter/ucp/2.2/guides/admin/install/#step-4-install-ucp

# Docker Namespace
![DockerNamespace](images/DockerNamespace.png)
It allows each container that own its hardware resources.

# Docker Control Group (CGroup)
![DockerCGroup](images/DockerCGroup.png)

# Back Up Docker
![BackupDocker](images/BackupDocker.png)

# Docker Volume
https://docs.docker.com/storage/volumes/

# Docker Storage Driver
https://docs.docker.com/storage/storagedriver/

# Docker Registry = DockerHub
https://hub.docker.com/

# Docker Installation - Ubuntu
```shell
# Uninstall old versions
sudo apt-get remove docker docker-engine docker.io containerd runc

# Update the apt package index
sudo apt-get update

# Install packages to allow apt to use a repository over HTTPS
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

# Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# Set up the stable repository
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install Docker Engine
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# Verify that Docker Engine is installed correctly
sudo docker run hello-world
```
Reference:
https://docs.docker.com/engine/install/ubuntu/

# How to build an image
```dockerfile
FROM node:12-alpine
RUN apk add --no-cache python g++ make
WORKDIR /app
COPY . .
RUN yarn install --production
CMD ["node", "src/index.js"]
```

then:

```shell
docker build -t getting-started .
```
-t = we want to tag the image
getting-started = the friendly name to identify the image
. = Make docker to look for the Dockerfile inside the current directory

```shell
docker run -dp 3000:3000 getting-started
```

Reference:
https://docs.docker.com/get-started/02_our_app/

# Documentation
## Docker Engine
https://docs.docker.com/engine/
## Docker Compose
https://docs.docker.com/compose/
01h52m_PLURALSIGHT_Developing ReactJS Apps with Docker
02h20m_PLURALSIGHT_Kubernetes for Developers Moving from Docker-Compose to Kubernetes
02h58m_PLURALSIGHT_Deploying Containerized Application
04h47m_PLURALSIGHT_Docker Deep Dive
05h19m_PLURALSIGHT_Docker for Web Developer
05h31m_PLURALSIGHT_Docker for Web Developer
05h41m_PLURALSIGHT_Getting Started with Docker on Windows
## Dockerfile
https://docs.docker.com/engine/reference/builder/
01h55m_PLURALSIGHT_Containerizing Angular Application with Docker
04h00m_PLURALSIGHT_Docker Images and Containers for ASP.NET
04h47m_PLURALSIGHT_Docker Deep Dive
05h31m_PLURALSIGHT_Docker for Web Developer 2020
## Compose-file
https://docs.docker.com/compose/compose-file/
## Docker CLI
https://docs.docker.com/engine/reference/commandline/cli/
## Docker-compose CLI
https://docs.docker.com/compose/reference/overview/
01h52m_PLURALSIGHT_Developing React.js Apps with Docker 2021 Saravanan
02h20m_PLURALSIGHT_Kubernetes for Developers Moving from Docker Compose to Kubernetes Dan Wahlin
02h58m_PLURALSIGHT_Deploying Containerized Applications 2021 Elton Stoneman
04h35m_Pluralsight - Docker Deep Dive by Nigel Poulton
## Docker Swarm CLI
https://docs.docker.com/engine/reference/commandline/swarm/
https://docs.docker.com/engine/swarm/
01h16m_PLURALSIGHT_Managing Docker on Windows Servers
02h05m_PLURALSIGHT_Getting Started With Docker
02h22m_PLURALSIGHT_Docker Swarm - Native Docker Clustering
02h58m_PLURALSIGHT_Deploying Containerized Application
04h47m_PLURALSIGHT_Docker Deep Dive
05h45m_PLURALSIGHT_Getting Started With Docker Swarm Mode

# Docker-compose example (docker-compose.yml)
```dockerfile
version: "3.7"

services:
  app:
    image: node:12-alpine
    command: sh -c "yarn install && yarn run dev"
    ports:
      - 3000:3000
    working_dir: /app
    volumes:
      - ./:/app
    environment:
      MYSQL_HOST: mysql
      MYSQL_USER: root
      MYSQL_PASSWORD: secret
      MYSQL_DB: todos

  mysql:
    image: mysql:5.7
    volumes:
      - todo-mysql-data:/var/lib/mysql
    environment:
      MYSQL_ROOT_PASSWORD: secret
      MYSQL_DATABASE: todos

volumes:
  todo-mysql-data:
```
Reference:q
https://docs.docker.com/get-started/08_using_compose/

For more example, I would recommend the following:
* Sample App with Compose: https://docs.docker.com/compose/samples-for-compose/
* Django: https://docs.docker.com/compose/django/
* Rails: https://docs.docker.com/compose/rails/
* Wordpress: https://docs.docker.com/compose/wordpress/
* MORE:https://github.com/docker/awesome-compose

# Docker Question-Answer
Q: What is Docker ?  
A: Docker provides the ability to package an application with all its dependencies into a Docker Image.
https://docs.docker.com/get-started/overview/

Q: What are the main component of Docker ?  
A: Docker use a Client-Server architecture. Its main components are:
1. Docker client (Docker CLI or Docker compose)
2. docker daemon (aka docker engine - dockerd)
3. Docker registry (mainly DockerHub)

Q: How does the docker client interacts with the docker daemon ?  
A: Via a REST API

Q: List all docker objects ?  
A: We have:
1. Image
2. Container
3. Network
4. Volume

Q: What is an image ?  
A:

Q: What is a container ?  
A:

Q: Image vs container ?  
A: a container is a runnable instance of an image.

Q: What is a network ?  
A: Docker’s networking subsystem is pluggable, using drivers.  
Reference: https://docs.docker.com/network/

Q: What are different types of drivers ?  
A:
1. Bridge
2. Host
3. Overlay
4. Macvlan
5. None

Summary:
* User-defined bridge networks are best when you need multiple containers to communicate on the same Docker host.
* Host networks are best when the network stack should not be isolated from the Docker host, but you want other aspects of the container to be isolated.
* Overlay networks are best when you need containers running on different Docker hosts to communicate, or when multiple applications work together using swarm services.
* Macvlan networks are best when you are migrating from a VM setup or need your containers to look like physical hosts on your network, each with a unique MAC address.
* Third-party network plugins allow you to integrate Docker with specialized network stacks.

Q: Demonstrate how 2 containers can talk to each other using a bridge network ?  
A:
```shell
# source:
# https://docs.docker.com/network/bridge/

###################################################################################################
# EXAMPLE 01
###################################################################################################
# First, start from a clean state
sudo docker container stop $(sudo docker container ls -aq)
sudo docker container prune --force && \
sudo docker image prune --force &&
sudo docker network prune --force && \
sudo docker volume prune --force && \
sudo docker system prune --force --all

# Create a network
sudo docker network create my-secret-network

# List our network
sudo docker network ls

# Create a container with our network
sudo docker run -it --rm --network my-secret-network --name my-secret-alpine alpine

# Show our container ip address
ifconfig

# Create another container with our network (in another terminal)
sudo docker run -it --rm --network my-secret-network --name my-secret-busybox busybox
ifconfig

# From Busybox, ping Alpine
ping [AlpineIpAddress]
# That is it! This is how to make 2 or more containers talk to each other.


# Open another terminal and run
sudo docker network inspect my-secret-network
# At the end, you should see our 2 containers:
"Containers": {
    "1ed7e399d81607dd9680e7aee44b82a766dad1df90d1bea4f6f7394bb5f717b9": {
        "Name": "my-secret-busybox", <-----
        "EndpointID": "f9813e7455f1d381d36379b0838a429f17ea297465b7d41d1395c45f94eb2c7e",
        "MacAddress": "02:42:ac:16:00:03",
        "IPv4Address": "172.22.0.3/16",
        "IPv6Address": ""
    },
    "93e63e5912761a6805db0903e0956e9217415fb6ca13fda2e1fb1dbbd5635641": {
        "Name": "my-secret-alpine", <-----
        "EndpointID": "6c4b44169a78ab0d081ca19ba5c7c02be95de5ad13700a76b51f1636b92685f0",
        "MacAddress": "02:42:ac:16:00:02",
        "IPv4Address": "172.22.0.2/16",
        "IPv6Address": ""
    }
},
###################################################################################################
# EXAMPLE 02
###################################################################################################
# First, start from a clean state
sudo docker container stop $(sudo docker container ls -aq)
sudo docker container prune --force && \
sudo docker image prune --force &&
sudo docker network prune --force && \
sudo docker volume prune --force && \
sudo docker system prune --force --all

# Create a network
sudo docker network create my-secret-network

# List our network
sudo docker network ls

# Create a container with our network
sudo docker run -d --rm --network my-secret-network --name my-secret-nginx nginx

# List our container (To seee that it is still running)
sudo docker container ls

# Find the IPv4 of our Nginx container
sudo docker inspect [NginxContainerID]
# Look at the "IPAddress"
# Example:
# Gateway": "172.17.0.1",
# "IPAddress": "172.17.0.2" <----- This is what we want

# Create another container with our network (in another terminal)
sudo docker run -it --rm --network my-secret-network --name my-secret-ubuntu ubuntu
apt update && apt install curl inetutils-ping --yes

# Verify that Ubuntu can talk to Nginx
curl [NginxIpAddress]
ping [NginxIpAddress]
# That is it! This is how to make 2 or more containers talk to each other.

# Open another terminal and run:
sudo docker network inspect my-secret-network
# At the end, you should see our 2 containers:
"Containers": {
    "ce344cf21bc01c04a71d4a529918d8d43febaf8c98073473b3af5b34b37c55e8": {
        "Name": "my-secret-nginx", <-----
        "EndpointID": "9aab38c4abc2837ac42700e2867ce3c8cc2d9527c88491c21e2d5bb12917f7cf",
        "MacAddress": "02:42:ac:17:00:02",
        "IPv4Address": "172.23.0.2/16",
        "IPv6Address": ""
    },
    "da099dcd1a94dee7f36dd98b8e498591ac6565bb3f44090345a53d970a7ff8a9": {
        "Name": "my-secret-ubuntu", <-----
        "EndpointID": "50efd851d4979a0ddfba0649e1ddc42c075fd8f7a62df619a2d3fbfc511bd6c1",
        "MacAddress": "02:42:ac:17:00:03",
        "IPv4Address": "172.23.0.3/16",
        "IPv6Address": ""
    }
},
```

For more tutorials, I recommend the following:
* Bridge network: https://docs.docker.com/network/network-tutorial-standalone/
* Host network: https://docs.docker.com/network/network-tutorial-host/
* Overlay network: https://docs.docker.com/network/network-tutorial-overlay/
* Macvlan network: https://docs.docker.com/network/network-tutorial-macvlan/

Q: What is a volume ?  
A:
Volumes provide the ability to connect specific filesystem paths of the container back to the host machine.
If a directory in the container is mounted, changes in that directory are also seen on the host machine.
If we mount that same directory across container restarts, we’d see the same files.
https://docs.docker.com/get-started/05_persisting_data/#container-volumes

Example
1. Download the app from: https://docs.docker.com/get-started/02_our_app/
2. Extract the archive
3. Create a Dockerfile and paste:
```dockerfile
 FROM node:12-alpine
 RUN apk add --no-cache python g++ make
 WORKDIR /app
 COPY . .
 RUN yarn install --production
 CMD ["node", "src/index.js"]
```
OR
```dockerfile
 FROM node:12-alpine
 RUN apk add --no-cache python g++ make
 WORKDIR /app
 COPY ./app .
 RUN yarn install --production
 CMD ["node", "src/index.js"]
```
4. Build the Dockerfile using:
sudo docker build --tag my-todo-app .

5. Run the image
sudo docker run -d --publish 3000:3000 my-todo-app

**NOTE** All items that we add will only be available inside that container.  
If we decide to delete that container using:

    sudo docker container rm <ContainerID> --force

We will be deleting the container with **ALL ITS DATA**.

The solution to have our data saved separate from our container is to use a volume:
```shell
# Create a named volume
sudo docker volume create todo-db

# Attach the volume when running a new container
sudo docker run -d --publish 3000:3000 --volume todo-db:/etc/todos --name first_instance my-todo-app
```

Q: Where is Docker actually storing my data when I use a named volume?  
A: 
1. run the "docker volume inspect <VolumeName>"
2. Look at the 'Mountpoint' value

Q: How to make 2 containers see each other ?  
A: using network
https://docs.docker.com/get-started/07_multi_container/#container-networking

Q: What technology was used to build Docker ?  
A: Golang programming language

# Docker Commands
| Commands | Description |
|----------|-------------|
docker version | show installed docker version
docker login | login to dockerhub
docker logout | logout from dockerhub
docker images | list local images
docker tag &lt;imageID&gt; &lt;tagName&gt; |
docker push &lt;YourDockerID&gt;/&lt;DockerRepo&gt; |
docker pull &lt;ImageName&gt;:&lt;Tag&gt; |
sudo systemctl status docker | show docker engine service status
sudo systemctl start docker | start docker engine service
sudo systemctl enable docker | make docker engine start on boot
|
docker-compose -f path/to/docker-compose.yml up --detach |
docker-compose -f path/to/docker-compose.yml stop <ServiceName> |
docker-compose -f path/to/docker-compose.yml start <ServiceName> |

# ==================================================
# Docker Swarm
## To initialize a cluster
```shell
sudo docker swarm init
```

```shell
sudo docker swarm --help
sudo docker info --help
sudo docker node --help
```

## List swarm nodes
sudo docker node ls

![HowNodeWork](images/HowNodeWork.png)
https://docs.docker.com/engine/swarm/how-swarm-mode-works/nodes/

## inspect a node
```shell
sudo docker node inspect self
sudo docker service --help
```

## Run a container inside a cluster
sudo docker service create --name web-server --publish 8080:80 nginx

Q: What is a service?
A: a definition of an application as a set of tasks.

![HowServiceWork](images/HowServiceWork.png)
https://docs.docker.com/engine/swarm/how-swarm-mode-works/services/

## Service Load Balancing
![SwarmRoutingMesh](images/SwarmRoutingMesh.png)
https://docs.docker.com/engine/swarm/ingress/

![SwarmIngressNetwork](images/SwarmIngressNetwork.png)

Q: When will you use "sudo docker swarm init --advertise-addr=<MyIpAddress" ?
A: Usually, with VirtualBox, there are 2 network interface card = 2 ipv4, you will need to explicitly input to address.

sudo docker service inspect --help
sudo docker service inspect <TaskName>

sudo docker service rm <ServiceName>

## List tasks inside a service
sudo docker service ps

## to update/scale a service to 4
sudo docker service update --replicas=4 <TaskName>
sudo docker service scale <TaskName>=4

## To move all tasks running on a node for maintenance
sudo docker node update --availability=drain <NodeName>
